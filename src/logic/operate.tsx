import Big from "big.js";
import { INFINITY, ACTIONS } from "../constants";

export default function operate(
  numberOne: string,
  numberTwo: string,
  operation: string
) {
  const one = Big(numberOne || "0");
  //If dividing or multiplying, then 1 maintains current value in cases of null
  const two: any = Big(
    numberTwo || (operation === ACTIONS.DIVISION || operation === ACTIONS.MULTIPLICATION ? "1" : "0")
  );

  switch(operation) {
    case ACTIONS.ADDITION: return one.plus(two).toString();
    case ACTIONS.SUBSTRACTION: return one.minus(two).toString();
    case ACTIONS.MULTIPLICATION: return one.times(two).toString();
    case ACTIONS.DIVISION: {
      if (String(two["c"][0]) === "0") {
        return String(INFINITY);
      } else {
        return one.div(two).toString();
      }
    }
    default: throw Error(`Unknown operation '${operation}'`);
  }
}
